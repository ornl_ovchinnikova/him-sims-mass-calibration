import os
import numpy as np
import _tkinter
import tkinter
import matplotlib.pyplot as plt
from scipy import signal

def apply_polynomial(x, p):
    '''
    Passes x data through an n-degree polynomial function with coefficients p.
    Infers degree of polynomial from number of values in p.

    Input:
    --------
        x : 1D numpy array or list
        data to be fed through polynomial function

        p : tuple or list
        polynomial coefficients, in order from highest to lowest order
        (ie for the quadratic equation y = 2x^2 + 5x + 3, p = [2, 5, 3])

    Output:
    --------
        y : 1D numpy array
        result of each value in x being passed through the function specified by p
    '''
    y = np.empty_like(x)
    # Infer polynomial order from number of passed coefficients
    order = len(p) - 1
    for idx, value in enumerate(x):
        result = 0
        d = 0
        while d <= order:
            #Iteratively add contribution of each coefficient until all coefficients have been added
            coefficient = p[d]
            exponent = order-d
            result += coefficient*(value**exponent)
            d += 1
            y[idx] = result
    return y

def fit_distribution(x, y, p=(1,1,1), dist='gaussian'):
    '''
    Fit a statistical distribution to data y using initial guess parameters p
    
    Input:
    --------
        x : array-like
            x-axis for data to be fitted
        y : array-like
            y-axis for data to be fitted
        p : tuple
            initial guess of fitting parameters
        dist : string
            statistical distribution to be fitted to data
    
    Output:
    --------
        fitted : numpy.array
            y-values of fitted data
        popt : tuple
            tuple containing optimized fitting parameters
    '''
    from scipy.optimize import curve_fit
    from scipy import exp
    
    def _gaussian_(x, a, x0, sigma):
        '''
        In probability theory, the normal (or Gaussian or Gauss or Laplace–Gauss)
        distribution is a very common continuous probability distribution. Normal
        distributions are important in statistics and are often used in the 
        natural and social sciences to represent real-valued random variables
        whose distributions are not known. A random variable with a Gaussian 
        distribution is said to be normally distributed and is called a normal deviate.
        '''
        return a*exp(-(x-x0)**2/(2*sigma**2))
    def _laplace_(x, a, mu, sigma):
        '''
        In probability theory and statistics, the Laplace distribution is a
        continuous probability distribution named after Pierre-Simon Laplace.
        It is also sometimes called the double exponential distribution, because
        it can be thought of as two exponential distributions (with an additional
        location parameter) spliced together back-to-back, although the term is
        also sometimes used to refer to the Gumbel distribution. The difference
        between two independent identically distributed exponential random
        variables is governed by a Laplace distribution, as is a Brownian motion
        evaluated at an exponentially distributed random time. Increments of
        Laplace motion or a variance gamma process evaluated over the time scale
        also have a Laplace distribution.
        '''
        return a*(np.exp(-(np.abs(x-mu))/sigma)/(2*sigma))
    def _cauchy_(x, a, x0, hwhm):
        '''
        The Cauchy distribution, named after Augustin Cauchy, is a continuous
        probability distribution. It is also known, especially among physicists,
        as the Lorentz distribution (after Hendrik Lorentz), Cauchy–Lorentz
        distribution, Lorentz(ian) function, or Breit–Wigner distribution. The
        Cauchy distribution is the distribution of the x-intercept of a ray 
        issuing from (x0, hwhm) with a uniformly distributed angle. It is also
        the distribution of the ratio of two independent normally distributed
        random variables if the denominator distribution has mean zero.
        '''
        return a*((hwhm**2 / ((x-x0)**2 + hwhm**2))*(1/x0/np.pi))
    
    dist_options = set(['gaussian', 'laplace', 'cauchy'])
    # Select distribution from available options
    if dist in dist_options:
        if dist.upper() == 'GAUSSIAN':
            popt, pcov = curve_fit(_gaussian_, x, y, p)
            fitted = np.array([_gaussian_(i,p[0],p[1],p[2]) for i in x])
        elif dist.upper() == 'LAPLACE':
            popt, pcov = curve_fit(_laplace_, x, y, p)
            fitted = np.array([_laplace_(i,p[0],p[1],p[2]) for i in x])
        elif dist.upper() == 'CAUCHY':
            popt, pcov = curve_fit(_cauchy_, x, y, p)
            fitted = np.array([_cauchy_(i,p[0],p[1],p[2]) for i in x])
        else:
            raise KeyError('Selected distribution is not a valid option.')
            fitted = popt = None
    else: fitted = popt = None
    return fitted, popt

def find_nearest_member(container, query):
    '''
    Finds the member of a container whose value is nearest to query. Returns
    index of nearest value within container. Intended to be used when
    list.index(query) is, for whatever reason, not a viable option for locating
    the desired value within the container.

    Input:
    --------
    container : container variable (eg list, tuple, set, Numpy array)
        The container to be searched by the function
    query : number (eg int or float)
        Value to be searched for within container

    Output:
    --------
    mindex : int
        Index of item in container whose value most nearly matches query
    '''
    # Calculate distance of query to each point in container
    try:
        diffs = abs(container - query)
    except:
        diffs = []
        for entry in container:
            difference = entry - query
            diffs.append(abs(difference))
    # Minimum distance value is selected as the value in container where distance between value and query are minimized
    minimum = min(diffs)
    mindex = list(diffs).index(minimum)
    return mindex

def soft_append(container, addendum):
    '''
    Appends addendum item to container only if addendum is not already member
    of container. Returns nothing, since container is appended in-place.

    Input:
    --------
    container : list
        container object to be appended
    addendum : any
        value to be soft-appended to container
    '''
    if addendum not in container:
        container.append(addendum)
        return
    return

def cartesian_distance(a, b):
    '''
    Calculates the distance between two points within a cartesian coordinate plane

    Input:
    --------
        a : tuple or list
        First point to consider for distance calculation

        b : tuple or list
        Second point to consider for distance calculation

    Output:
    --------
        distance : float
        Distance between points a and b, expressed in the same units as the coordinates given for a and b
    '''
    import numpy as np
    x1, y1 = a
    x2, y2 = b
    distance = np.sqrt((x2-x1)**2+(y2-y1)**2)
    return distance

def peak_resolution(center, width):
   return center/width

class HimSimsData:
    '''
    Class for organizing and manipulating imported HIM-SIMS data
    '''
    def __init__(self, n_channels=3):
        self.n_channels = n_channels
        self.positions = np.array([[] for i in range(n_channels)])
        self.intensities = np.array([[] for i in range(n_channels)])
        self.master_mz = False
        self.master_int = False
        self.calibrated = False
        self.merged_mz = False
        self.merged_int = False
        self.metadata = False
        return

class CoRegistrationGUI:
    '''
    GUI window for guided calibration and co-registration of HIM-SIMS mass spectra
    '''
    def __init__(self, master):
        '''
        Initialize GUI window, place buttons and fields

        Input:
        --------
            master : tkinter.Tk
            root tkinter object, must be initialized outside of CoRegistrationGUI
        '''
        # Initialize data handler
        self.data = HimSimsData(3)
        # Initialize GUI window
        self.master = master
        self.master.title('HIM-SIMS calibration + co-registration')
        # Spacer is included only for visual formatting of the master window
        self.spacer = tkinter.Label(master, text='')
        # Data entry and readout fields
        self.data_dir_entry_field = tkinter.Entry(master)
        self.readout_field = tkinter.Label(master, text='waiting for data import')
        self.dat_fname_field = tkinter.Entry(master)
        self.bin_size_field = tkinter.Entry(master)
        # Various buttons and menus
        self.data_import_button = tkinter.Button(master, text='Import data', command=self.import_data)
        self.calibrate_channel_button = tkinter.Button(master, text='Initial calibration', command=self.calibrate_channels)
        self.align_button = tkinter.Button(master, text='Align spectra', command=self.align_spectra)
        self.skip_button = tkinter.Button(master, text='Skip alignment', command=self.skip_align)
        self.merge_button = tkinter.Button(master, text='Merge spectra', command=self.merge_channels)
        self.merge_mode = tkinter.StringVar(master)
        self.merge_mode.set('Binned average')
        merge_mode_field = tkinter.OptionMenu(master, self.merge_mode,
                                                 'Binned average',
                                                 'Savitzky-Golay').grid(row=6,column=0)
        self.calibrate_button = tkinter.Button(master, text='Calibrate spectra', command=self.calibrate_spectra)
#        self.canvas_button = tkinter.Button(master, text='Open canvas', command=self.canvas_window)
        self.export_button = tkinter.Button(master, text='Export data', command=self.export_data)
        # Place various elements into window
        self.data_dir_entry_field.grid(row=0,column=1)
        tkinter.Label(master, text='Data Directory').grid(row=0,column=0)
        tkinter.Label(master, text='.dat Filename').grid(row=1,column=0)
        self.dat_fname_field.grid(row=1,column=1)
        self.data_import_button.grid(row=1,column=2)
        self.spacer.grid(row=2, column=1)
        self.spacer.grid(row=3, column=1)
#        self.calibrate_channel_button.grid(row=4,column=1)
        tkinter.Label(master, text='n_bins:').grid(row=5, column=2)
        self.merge_button.grid(row=6, column=1)
        self.align_button.grid(row=5, column=1)
#        self.skip_button.grid(row=5, column=0)
        self.bin_size_field.grid(row=6, column=2)
        self.calibrate_button.grid(row=7, column=1)
        self.readout_field.grid(row=9, column = 0)
#        tkinter.Label(master, text='Calibrant masses').grid(row=5, column=0)
        return

    def import_data(self):
        '''
        Import data from user-specified .dat file. Organizes and outputs
        results to self.data (a HimSimsData object)
        '''
        def close_fig(event):
            '''
            Helper function to be called by matplotlib interactive interface
            Do not call as standalone function!
            '''
            plt.close()
            fig.canvas.mpl_disconnect(cid)
            return
        
        self.readout_field['text'] = 'importing raw data...'
        # Read and extract data directory and filename from main GUI window
        data_dir = self.data_dir_entry_field.get()
        self.data_dir = data_dir
        dat_fname = self.dat_fname_field.get()
        split_name = dat_fname.split('.')
        if len(split_name) == 1:
            self.dat_fname = dat_fname
            dat_fname = dat_fname+'.dat'
        else:
            self.dat_fname = split_name[0]
        dat_fname = os.path.join(data_dir, dat_fname)
        #Dump dat file contents to list
        with open(dat_fname, 'r') as f:
            lines = [line for line in f]
        #Organize and reformat dat file contents
        meta = lines[:8]
        for idx, row in enumerate(meta):
            chars = [c for c in row]
            row = ''
            for iidx, c in enumerate(chars):
                if c == ',':
                    c = ';'
                row = row + c
            row = row.split(';')
            while '' in row:
                row.remove('')
            while '\n' in row:
                row.remove('\n')
            meta[idx] = row
        self.data.metadata = meta
        header = lines[8]
        chars = [c for c in header]
        row = ''
        for iidx, c in enumerate(chars):
            if c == ',':
                c = ';'
            row = row + c
        row = row.split(';')
        while '' in row:
            row.remove('')
        while '\n' in row:
            row.remove('\n')
        header = row
        self.data.header = header
        body = lines[9:]
        for idx, row in enumerate(body):
            chars = [c for c in row]
            row = ''
            for iidx, c in enumerate(chars):
                if c == ',':
                    c = ';'
                row = row + c
            row = row.split(';')
            while '' in row:
                row.remove('')
            while '\n' in row:
                row.remove('\n')
            body[idx] = row
        self.data.body = body
        n_rows = len(body)
        pos_indices = [None, None, None]
        int_indices = [None, None, None]
        # Identify data columns for each channel within .dat file dump
        for idx, item in enumerate(header):
            item = item.split(' ')
            if 'Atomic' in item:
                if 'CH1' in item:
                    pos_indices[0] = idx
                    continue
                if 'CH2' in item:
                    pos_indices[1] = idx
                    continue
                if 'CH3' in item:
                    pos_indices[2] = idx
                    continue
            if '(cps)' in item:
                if 'CH1' in item:
                    int_indices[0] = idx
                    continue
                if 'CH2' in item:
                    int_indices[1] = idx
                    continue
                if 'CH3' in item:
                    int_indices[2] = idx
                    continue
        # Read in channel position and intensity measurements, save to HimSimsData object
        position_data = np.empty((3, n_rows))
        for idx, column in enumerate(pos_indices):
            col_pos = np.empty(n_rows)
            for iidx, line in enumerate(body):
                col_pos[iidx] = float(line[column])
            position_data[idx] = col_pos
        intensity_data = np.empty((3, n_rows))
        for idx, column in enumerate(int_indices):
            intensity_data[idx] = np.array([line[column] for line in body])
        self.data.positions = position_data
        self.data.intensities = intensity_data
        normalized_intensities = np.array([(ints - np.min(ints))/np.max(ints) for ints in intensity_data])
        # Show imported channels as figure
        fig, ax = plt.subplots(self.data.n_channels, sharex=True, sharey=True)
        plt.xlabel('m/z')
        color_schema = ['b','g','r']
        for i in range(self.data.n_channels):
            ax[i].plot(self.data.positions[i], self.data.intensities[i], color=color_schema[i])
            ax[i].set_title('Channel %i'%(i+1))
        plt.show()
        self.readout_field['text'] = 'data import complete'
        # Allows figure window to be closed by clicking anywhere within figure window
        cid = fig.canvas.mpl_connect('button_release_event', close_fig)
        # Add data export button to GUI main window
        self.export_button.grid(row=9, column=2)
        return
    
    def export_data(self):
        '''
        Exports all generated result data to a formatted Microsoft Excel .xlsx
        workbook file. Each step of processing (import, alignment, merging, global
        calibration) is given a dedicated worksheet within the workbook file.
        Exported data includes m/z and intensity axes resulting from each processing
        step, as well as various metadata specific to each process (ex. calibration
        coefficients and R^2 values for global mass calibration).
        '''
        import openpyxl
        from openpyxl import Workbook
        import scipy.stats as stats
        
        def rmse(a,b):
            '''
            Calculates the root mean-square error between an experimental and
            target dataset. Either a or b may be either the target or experimental
            dataset. 
            '''
            a = np.array(a)
            b = np.array(b)
            rmse = np.sqrt(((a - b) ** 2).mean())
            return rmse
            
        #Check processing progress
        self.readout_field['text'] = 'exporting data...'
        # Initialize flags to determine which pages of workbook to export
        aligned = False
        merged = False
        calibrated = False
        if type(self.data.master_mz) != bool:
            # self.data.master_mz is initialized to False. If it is not False, alignment has been performed
            if self.data.anchor_peaks_c1_c2 != None:
                aligned = True
        if type(self.data.merged_mz) != bool:
            # self.data.merged_mz is initialized to False. If it is not False, merging has been performed
            merged = True
        if type(self.data.calibrated) != bool:
            # self.data.calibrated is initialized to False. If it is not False, global calibration has been performed
            calibrated = True
        #Initialize workbook
        wb = Workbook()
        raw_sheet = wb.active
        #Write raw data to workbook
        raw_sheet.title = 'RawData'
        meta = self.data.metadata
        meta_rows = len(meta)
        # Copy and write metadata lines from original .dat file
        for i in range(meta_rows):
            meta_line = meta[i]
            line_len = len(meta_line)
            for j in range(line_len):
                c = raw_sheet.cell(row=i+1, column=j+1, value=meta_line[j])
        header_line = i + 2
        header = self.data.header
        header_len = len(header)
        for j, entry in enumerate(header):
            c = raw_sheet.cell(row=header_line, column=j+1, value=entry)
        # Copy and write raw mz and intensity values from original .dat file
        body = self.data.body
        body_start_row = header_line+1
        body_rows = len(body)
        for i, row in enumerate(body):
            i += body_start_row
            row_len = len(row)
            for j in range(row_len):
                c = raw_sheet.cell(row=i, column=j+1, value=row[j])
        #Write aligned spectra to workbook, if alignment has been performed
        if aligned:
            #Organize data, setup new sheet
            aligned_positions = self.data.positions
            aligned_intensities = self.data.intensities
            alignment_coefficients = [self.data.ch1_ch2_alignment_coefficients, self.data.ch2_ch3_alignment_coefficients]
            anchor_peaks = [self.data.anchor_peaks_c1_c2, self.data.anchor_peaks_c2_c3]
            header=['Atomic mass CH1', 'CH1 (cps)',
                'Atomic mass CH2', 'CH2 (cps)',
                'Atomic mass CH3', 'CH3 (cps)']
            position_indices = [0,2,4]
            intensity_indices = [1,3,5]
            aligned_sheet = wb.create_sheet('AlignedData')
            #Write header
            for i, item in enumerate(header):
                c = aligned_sheet.cell(row=1, column=i+1, value=item)
            #Write m/z and intensity data
            for i in range(3):
                positions = aligned_positions[i]
                intensities = aligned_intensities[i]
                pos_idx = position_indices[i] + 1
                int_idx = intensity_indices[i] + 1
                for j in range(len(positions)):
                    p = positions[j]
                    intensity = intensities[j]
                    j += 2
                    c = aligned_sheet.cell(row=j, column=pos_idx, value=p)
                    c = aligned_sheet.cell(row=j, column=int_idx, value=intensity)
            #Write measurement metadata header
            c = aligned_sheet.cell(row=1, column=8, value='CH1_CH2')
            c = aligned_sheet.cell(row=2, column=9, value='anchor_x1')
            c = aligned_sheet.cell(row=2, column=10, value='anchor_x2')
            c = aligned_sheet.cell(row=2, column=11, value='fit_param1')
            c = aligned_sheet.cell(row=2, column=12, value='fit_param2')
            c = aligned_sheet.cell(row=4, column=11, value='R^2')
            #Draw alignment figure
            plt.figure()
            n_anchor = 1
            coeffs = alignment_coefficients[0]
            anchors = anchor_peaks[0]
            x1s = [anchors[0][i][0] for i in range(len(anchors[0]))]
            x2s = [anchors[1][i][0] for i in range(len(anchors[0]))]
            x2s.sort()
            x1s.sort()
            #Write Ch1/Ch2 metadata
            for i in range(len(anchors[0])):
                c = aligned_sheet.cell(row=2+n_anchor, column=8, value=n_anchor)
                c = aligned_sheet.cell(row=2+n_anchor, column=9, value=x1s[i])
                c = aligned_sheet.cell(row=2+n_anchor, column=10, value=x2s[i])
                n_anchor +=1
            plt.scatter(x1s, x2s, color='b')
            r, prob = stats.pearsonr(x1s, x2s)
            r_squared = r**2
            c = aligned_sheet.cell(row=5, column=11, value=r_squared)
            c = aligned_sheet.cell(row=3, column=11, value=coeffs[0])
            c = aligned_sheet.cell(row=3, column=12, value=coeffs[1])
            bounds = (aligned_positions[1][0], aligned_positions[0][-1])
            fit_line_x = np.linspace(bounds[0], bounds[1], 5)
            fit_line_y = apply_polynomial(fit_line_x, coeffs)
            plt.plot(fit_line_x, fit_line_y, color='r')
            plt.savefig('fit_temp1.png')
            plt.close()
            img = openpyxl.drawing.image.Image('fit_temp1.png')
            c = aligned_sheet.cell(row=n_anchor+4, column=8)
            aligned_sheet.add_image(img, 'H25')
            #Write Ch2/Ch3 metadata
            c = aligned_sheet.cell(row=1, column=13, value='CH2_CH3')
            c = aligned_sheet.cell(row=2, column=14, value='anchor_x1')
            c = aligned_sheet.cell(row=2, column=15, value='anchor_x2')
            c = aligned_sheet.cell(row=2, column=16, value='fit_param1')
            c = aligned_sheet.cell(row=2, column=17, value='fit_param2')
            c = aligned_sheet.cell(row=4, column=16, value='R^2')
            plt.figure()
            n_anchor = 1
            coeffs = alignment_coefficients[1]
            anchors = anchor_peaks[1]
            x1s = [anchors[0][i][0] for i in range(len(anchors[0]))]
            x2s = [anchors[1][i][0] for i in range(len(anchors[0]))]
            x2s.sort()
            x1s.sort()
            r, prob = stats.pearsonr(x1s, x2s)
            r_squared = r**2
            for i in range(len(anchors[0])):
                c = aligned_sheet.cell(row=2+n_anchor, column=13, value=n_anchor)
                c = aligned_sheet.cell(row=2+n_anchor, column=14, value=x1s[i])
                c = aligned_sheet.cell(row=2+n_anchor, column=15, value=x2s[i])
                n_anchor +=1
            plt.scatter(x1s, x2s, color='b')
            bounds = (aligned_positions[2][0], aligned_positions[1][-1])
            c = aligned_sheet.cell(row=5, column=16, value=r_squared)
            c = aligned_sheet.cell(row=3, column=16, value=coeffs[0])
            c = aligned_sheet.cell(row=3, column=17, value=coeffs[1])
            fit_line_x = np.linspace(bounds[1], bounds[0], 5)
            fit_line_y = apply_polynomial(fit_line_x, coeffs)
            plt.plot(fit_line_x, fit_line_y, color='r')
            plt.savefig('fit_temp2.png')
            plt.close()
            img = openpyxl.drawing.image.Image('fit_temp2.png')
            c = aligned_sheet.cell(row=n_anchor+4, column=8)
            aligned_sheet.add_image(img, 'R25')
            
        #Write merged channel data, if merging has been performed
        if merged:
            merged_sheet = wb.create_sheet('MergedData')
            merged_int = self.data.merged_int
            merged_mz = self.data.merged_mz
            merge_mode = self.data.merge_mode
            # Write header data to sheet
            header = ['Atomic mass (m/z)', 'Intensity (cps)',
                      'mode']
            for i, item in enumerate(header):
                c = merged_sheet.cell(row=1, column=i+1, value=item)
            n_samples = len(merged_int)
            c = merged_sheet.cell(row=2, column=3, value=merge_mode)
            # Write merged m/z and intensity datasets
            for i in range(n_samples):
                intensity = merged_int[i]
                mz = merged_mz[i]
                i += 2
                c = merged_sheet.cell(row=i, column=1, value=mz)
                c = merged_sheet.cell(row=i, column=2, value=intensity)
            # Write merging metadata
            merge_parms = self.data.merge_parms
            col = 4
            for key, value in merge_parms.items():
                c = merged_sheet.cell(row=1, column=col, value=key)
                c = merged_sheet.cell(row=2, column=col, value=value)
                col += 1
            # Write measurements of major peaks within merged dataset
            merged_sheet.cell(10, 4, 'Peaks')
            keys = [key for key in self.data.peaks.keys()]
            values = [value for value in self.data.peaks.values()]
            merged_peak_params = {keys[i]:values[i] for i in range(len(keys))}
            for idx, (key, value) in enumerate(merged_peak_params.items()):
                idx += 4
                merged_sheet.cell(11, idx, key)
                row = 12
                for entry in value:
                    merged_sheet.cell(row, idx, entry)
                    row += 1
        #Write mass-calibrated data, if global mass calibration has been performed
        if calibrated:
            calibrated_sheet = wb.create_sheet('CalibratedData')
            calibrated_int = self.data.merged_int
            calibrated_mz = self.data.calibrated
            # Write header lines
            header = ['Atomic mass (m/z)', 'Intensity (cps)',
                      'Calibration Peaks (m/z)', 'Lock Masses (m/z)']
            for i, item in enumerate(header):
                c = calibrated_sheet.cell(row=1, column=i+1, value=item)
            n_samples = len(calibrated_int)
            # Write calibrated m/z and intensity data
            for i in range(n_samples):
                intensity = calibrated_int[i]
                mz = calibrated_mz[i]
                i += 2
                c = calibrated_sheet.cell(row=i, column=1, value=mz)
                c = calibrated_sheet.cell(row=i, column=2, value=intensity)
            # Write lock mass metadata and alignment parameters
            x, y = self.data.calibration_params['anchors']
            for i in range(len(x)):
                a = x[i]
                b = y[i]
                i+=2
                c = calibrated_sheet.cell(row=i, column=3, value=a)
                c = calibrated_sheet.cell(row=i, column=4, value=b)
            col = 5
            for key, value in self.data.calibration_params.items():
                if key == 'anchors':
                    continue
                elif key == 'coeffs':
                    c = calibrated_sheet.cell(row=1,column=col,value=key)
                    r = 2
                    for coeff in value:
                        c = calibrated_sheet.cell(row=r, column=col, value=coeff)
                        r+=1
                else:
                    c = calibrated_sheet.cell(row=1,column=col,value=key)
                    c = calibrated_sheet.cell(row=2,column=col,value=value)
                col += 1
            # Write measurements of major peaks within calibrated data
            calibrated_sheet.cell(14, 6, 'Peaks')
            cal_coeffs = self.data.calibration_params['coeffs']
            keys = [key for key in self.data.calibrated_peaks.keys()]
            values = [value for value in self.data.calibrated_peaks.values()]
            merged_peak_params = {keys[i]:values[i] for i in range(len(keys))}
            for idx, (key, value) in enumerate(merged_peak_params.items()):
                idx += 6
                calibrated_sheet.cell(15, idx, key)
                row = 16
                for entry in value:
                    calibrated_sheet.cell(row, idx, entry)
                    row += 1
            plt.figure()
            plt.title('Lock masses vs. Calibration peak masses')
            plt.plot(self.data.calibrated, apply_polynomial(self.data.calibrated, cal_coeffs), color='b')
            for i in range(len(x)):
                plt.scatter(x[i],y[i],color='r')
            plt.savefig('calib_temp.png')
            plt.close()
            img = openpyxl.drawing.image.Image('calib_temp.png')
            calibrated_sheet.add_image(img, 'R15')
            
        #Save and close workbook
        xlsx_fname = self.dat_fname+'.xlsx'
        xlsx_fname = os.path.join(self.data_dir, xlsx_fname)
        wb.save(filename=xlsx_fname)
        if aligned:
            os.remove('fit_temp1.png')
            os.remove('fit_temp2.png')
        if calibrated:
            os.remove('calib_temp.png')
        self.readout_field['text'] = 'data export complete'
        return

    def align_spectra(self):
        '''
        Interactively aligns mass spectra by applying a 1st order linear correction
        based on user-specified peak masses. Aligns channel 1 and channel 2 to
        channel 3. Should be followed by 3rd order mass calibration.
        '''
        from scipy.optimize import curve_fit

        def _update_active_axes_(event):
            '''
            Updates self.active window to reflect the last matplotlib figure window
            which the cursor entered.
            '''
            inaxes = event.inaxes
            for i in range(self.data.n_channels):
                if inaxes == ax[i]:
                    self.active_window = i
                    return

        def pick_peaks(c1, c2, offset):
            '''
            Opens window displaying 2 overlapping sets of channel data. Waits for
            the user to select at least 2 peaks which appear in each displayed channel.
            These peaks are then used to perform 1st order m/z axis alignment.
            '''

            self.anchor_peaks = [[],[]]
            c = (c1, c2)

            def grab_peak(event):
                '''
                Extracts peak specified by user within matplotlib interactive 
                interface window.

                Input:
                -------
                    event : matplotlib.backend_bases.PickEvent
                    user interaction event signalling peak to be grabbed for calibration
                '''
                xpos = event.xdata
                ypos = event.ydata
                # If either x or y coordinate is given as an invalid data type, aborts peak grabbing
                if type(xpos) == type(None) or type(ypos) == type(None):
                    return
                click_pos = (xpos, ypos)
                # Extract a subsection of the axis 100 values in either direction from the click location
                nearest_x = find_nearest_member(c[self.active_window][0], xpos)
                target = 100
                lower = nearest_x - 100
                # If click location is less than 100 values from the left side of the plot, modify target index appropriately
                if lower < 0:
                    target += lower
                    lower = 0
                # Same for right side of plot
                upper = nearest_x + 100
                if upper > len(c[self.active_window][0]):
                    upper = len(c[self.active_window][0])
                extracted_window_prefilter = (c[self.active_window][0][lower:upper], c[self.active_window][1][lower:upper])
                # Smooth extracted window for enhanced peak identification
                extracted_window = signal.savgol_filter(extracted_window_prefilter, 7, 1)
                # Identify peaks within extracted window
                peaks, properties = signal.find_peaks(extracted_window[1], distance=15)
                # Identify peak nearest to click location
                nearest = peaks[find_nearest_member(peaks, target)]
                # Report click location and coordinates of corresponding nearest peak
                min_distance_peak = (extracted_window_prefilter[0][nearest], extracted_window_prefilter[1][nearest])
#                self.sel_peaks_text.set("test")
#                self.sel_peaks_text=selections['text']+'\n  %i  :       %5f        '%(self.active_window + 1 + offset, min_distance_peak[0])
#                selections['text'] = self.sel_peaks_text
                # Append nearest peak to list of anchor peaks for alignment
                print('channel: %i    click: %5f    peak: %5f'%(self.active_window+offset+1, click_pos[0], min_distance_peak[0]))
                self.anchor_peaks[self.active_window].append(min_distance_peak)
                return

            def _update_active_axes_(event):
                '''
                Monitors mouse movement to identify which subplot of the matplotlib window
                is being clicked, so that the correct channel can be used for peak grabbing
                '''
                inaxes = event.inaxes
                for i in range(2):
                    if inaxes == ax[i]:
                        self.active_window = i
                        return
                    
            def clear_anchors(offset):
                '''
                Clears all previously selected anchor peaks without closing any windows
                or requiring repeat of previous peak selection procedure
                '''
                self.anchor_peaks = [[], []]
                print('Channel %i, %i selections cleared!'%(1+offset, 2+offset))
                return
            
            def continue_align(fignum):
                
                pass
            
            # Initialize new calib window, print instructional text to new window
            guidetext = "Please select at least two peaks from the plot\nto use for channel alignment. Selected\npeaks must appear in both channels\n for accurate alignment.\n\nPlease click the 'Continue' button below when\nfinished selecting peaks to continue."
            
            calib = tkinter.Toplevel(self.master)
            
            fig, ax = plt.subplots(2, sharex=True, sharey=True)
            fignum = fig.number
            guide = tkinter.Label(calib, text=guidetext)
            continue_button = tkinter.Button(calib, text='Continue', command=calib.destroy)
            clear_button = tkinter.Button(calib, text='Clear selections', command=lambda : clear_anchors(offset))
            continue_button.grid(row=3, column=1)
            clear_button.grid(row=2, column=1)
            
            guide.grid(row=1,column=1)
            # Plot channel spectra
            
            self.wait = True
            plt.xlabel('m/z')
            color_scheme = ['b','g','r']
            ax[0].plot(c1[0], c1[1], color=color_scheme[0+offset])
            ax[0].xaxis.set_major_locator(plt.MultipleLocator(5))
            ax[0].xaxis.set_minor_locator(plt.MultipleLocator(1))
            ax[1].plot(c2[0], c2[1], color=color_scheme[1+offset])
            ax[0].set_title('Channel %i'%(1+offset))
            ax[1].set_title('Channel %i'%(2+offset))
            cid_1 = fig.canvas.mpl_connect('axes_enter_event', _update_active_axes_)
            cid_2 = fig.canvas.mpl_connect('button_release_event', grab_peak)
            plt.show()
            calib.wait_window(calib)
            if plt.fignum_exists(fignum):
                    plt.close()
            return self.anchor_peaks
        
        self.readout_field['text'] = 'performing channel alignment...'
        self.active_window = None
        full_anchor_list = False
        c1 = (self.data.positions[0], self.data.intensities[0])
        c2 = (self.data.positions[1], self.data.intensities[1])
        c3 = (self.data.positions[2], self.data.intensities[2])
#        self.sel_peaks_text= tkinter.StringVar()
#        self.sel_peaks_text.set('Channel     :      m/z\n---------------------------------')
#        sel_window = tkinter.Toplevel(self.master)
#        selections = tkinter.Label(sel_window, textvariable=self.sel_peaks_text)
#        selections.grid(row=4, column=1)
        # Select peaks in channels 1 and 2
        anchor_peaks_c1_c2 = pick_peaks(c1, c2, 0)
        self.data.anchor_peaks_c1_c2 = anchor_peaks_c1_c2
        x1, x2 = [x for (x,y) in anchor_peaks_c1_c2[0]], [x for (x,y) in anchor_peaks_c1_c2[1]]
        x1.sort()
        x2.sort()
        if len(x1) < 2 or len(x2) < 2:
            print('Selected too few peaks for alignment, aborting.')
            plt.close()
            return
        # Align channels 1 and 2
        
        p1 = np.polyfit(x1, x2, 1)
        self.data.ch1_ch2_alignment_coefficients = p1
        calibrated_channel_1 = apply_polynomial(self.data.positions[0], p1)
        # Select peaks in channels 2 and 3
        anchor_peaks_c2_c3 = pick_peaks(c2, c3, 1)
#        sel_window.destroy()
        self.data.anchor_peaks_c2_c3 = anchor_peaks_c2_c3
        x1, x2 = [x for (x,y) in anchor_peaks_c2_c3[0]], [x for (x,y) in anchor_peaks_c2_c3[1]]
        x1.sort()
        x2.sort()
        # Align channels 2 and 3
        p2 = np.polyfit(x2, x1, 1)
        self.data.ch2_ch3_alignment_coefficients = p2
        calibrated_channel_3 = apply_polynomial(self.data.positions[2], p2)
        calibrated_channel_2 = self.data.positions[1]
        calibrated_channels = [calibrated_channel_1,calibrated_channel_2,calibrated_channel_3]
        # Generate master mz and intensity arrays for use in merging and calibration
        self.data.master_mz = set()
        for channel in calibrated_channels:
            for entry in channel:
                self.data.master_mz.add(entry)
        self.data.master_mz = list(self.data.master_mz)
        self.data.master_mz.sort()
        self.data.master_int = np.empty_like(self.data.master_mz)
        for idx, mz in enumerate(self.data.master_mz):
            indices = []
            for i in range(3):
                try:
                    indices.append((list(calibrated_channels[i]).index(mz), i))
                except ValueError:
                    pass
            appears_in = [y for (x,y) in indices]
            sum_intensity = 0
            for i, j in enumerate(appears_in):
                sum_intensity += self.data.intensities[j][indices[i][0]]
            average_intensity = sum_intensity / (i+1)
            self.data.master_int[idx] = average_intensity
        self.data.master_mz = np.array(self.data.master_mz)
        self.data.positions = calibrated_channels
        # Display aligned channel spectrum
        plt.figure()
        plt.xlabel('m/z')
        color_scheme = ['b','g','r']
        for i in range(3):
            plt.plot(calibrated_channels[i], self.data.intensities[i], color=color_scheme[i])
        plt.show()
        self.readout_field['text'] = 'channel alignment complete'
        return
    
    def calibrate_channels(self):
        '''
        Apply lock-mass calibration to individual detector channels.
        Intended to be optionally called only prior to channel alignment.
        
        Largely a copy/paste of the global lock-mass calibration applied as 
        the last processing step.
        '''
        
        def grab_peak(event):
            '''
            Fits gaussian to peak specified by user within matplotlib
            interactive interface window.

            Input:
            -------
            event : matplotlib.backend_bases.PickEvent
            user interaction event signalling peak to be grabbed for calibration
            '''
            xpos = event.xdata
            ypos = event.ydata
            # If either x or y coordinate is given as an invalid data type, aborts peak grabbing
            if ypos < 0: return
            if type(xpos) == type(None) or type(ypos) == type(None):
                return 
            click_pos = (xpos, ypos)
            # Extract a subsection of the axis 100 values in either direction from the click location
            nearest_x = find_nearest_member(mz, xpos)
            target = 100
            lower = nearest_x - 100
            # If click location is less than 100 values from the left side of the plot, modify target index appropriately
            if lower < 0:
                target += lower
                lower = 0
            # Same for right side of plot
            upper = nearest_x + 100
            if upper > len(intensity): upper = len(intensity)
            extracted_window_prefilter = (mz[lower:upper], intensity[lower:upper])
            # Smooth extracted window for enhanced peak identification
            extracted_window = signal.savgol_filter(extracted_window_prefilter[1], 7, 1)
            # Identify major peaks within extracted window
            peaks, properties = signal.find_peaks(extracted_window, distance=15)
            # Identify major peak nearest to click location
            nearest = peaks[find_nearest_member(peaks, target)]
            # Report nearest peak coordinates
            min_distance_peak = (extracted_window_prefilter[0][nearest], extracted_window_prefilter[1][nearest])
            print('click: %5f    peak: %5f'%(click_pos[0], min_distance_peak[0]))
            # Append nearest peak to list of anchor peaks for calibration
            anchors.append(min_distance_peak)
            return

        def apply_calibration(mz, idx):
            import scipy.stats as stats
            
            plt.close()
            # Read user-specified lock masses from entry field, format PRN
            calibration_peaks = self.mass_entry_box.get(1.0, tkinter.END)
            calibration_peaks = calibration_peaks.split('\n')
            calibration_peaks.remove('')
            calibration_peaks = [float(mass) for mass in calibration_peaks]
            calibration_peaks.sort()
            x = [mass for (mass, intensity) in anchors]
            x.sort()
            x_i = [find_nearest_member(mz, mass) for mass in x]
            y = calibration_peaks
            # Fit to quadratic curve
            p = np.polyfit(x,y,2)
            check_masses = apply_polynomial(x, p)
            # Measure fitting parameters
            r, prob = stats.pearsonr(calibration_peaks, check_masses)
            # Apply calibration to channel data, save to HimSimsData object
            self.data.positions[idx] = apply_polynomial(mz, p)
            calibration.destroy()
            
        # Organize channel data
        c1 = (self.data.positions[0], self.data.intensities[0])
        c2 = (self.data.positions[1], self.data.intensities[1])
        c3 = (self.data.positions[2], self.data.intensities[2])
        
        channels = [c1,c2,c3]
        
        # Iterate through channel datasets, performing lock-mass calibration on each channel
        for idx, (mz, intensity) in enumerate(channels):
            anchors = []
            # Open new TopLevel window to manage calibration
            calibration = tkinter.Toplevel()
            self.data.channel_calibration_params = {}
            cal_mass_text = tkinter.StringVar()
            self.mass_entry_box = tkinter.Text(calibration, height=5, width=15)
            self.mass_entry_box.grid(row=0, column=0)
            self.calibrate_button = tkinter.Button(calibration, text='Calibrate!', command= lambda: apply_calibration(mz, idx))
            self.calibrate_button.grid(row=0, column=1)
            fig = plt.figure()
            plt.plot(mz, intensity)
            cid_2 = fig.canvas.mpl_connect('button_release_event', grab_peak)
            calibration.wait_window(calibration)
        
        
    def skip_align(self):
        '''
        Bypasses semi-automated channel alignment, to be used when channels either
        do not require alignment or have been manually aligned.
        '''
        # Organize channel data
        c1 = (self.data.positions[0], self.data.intensities[0])
        c2 = (self.data.positions[1], self.data.intensities[1])
        c3 = (self.data.positions[2], self.data.intensities[2])
        channels = [c1, c2, c3]
        # Collapse to 1D list of m/z and intensity values
        self.data.master_mz = set()
        for channel in channels:
            for entry in channel[0]:
                self.data.master_mz.add(entry)
        self.data.master_mz = list(self.data.master_mz)
        self.data.master_mz.sort()
        self.data.master_int = np.empty_like(self.data.master_mz)
        for idx, mz in enumerate(self.data.master_mz):
            indices = []
            for i in range(3):
                try:
                    indices.append((list(channels[i][0]).index(mz), i))
                except ValueError:
                    pass
            appears_in = [y for (x,y) in indices]
            sum_intensity = 0
            for i, j in enumerate(appears_in):
                sum_intensity += self.data.intensities[j][indices[i][0]]
            average_intensity = sum_intensity / (i+1)
            self.data.master_int[idx] = average_intensity
        # Place dummy values into required alignment metadata variables
        self.data.anchor_peaks_c1_c2 = None
        self.data.anchor_peaks_c2_c3 = None
        self.data.master_mz = np.array(self.data.master_mz)
        self.data.ch1_ch2_alignment_coefficients = (np.nan, np.nan)
        self.data.ch2_ch3_alignment_coefficients = (np.nan, np.nan)
        # Plot "aligned" datasets
        plt.figure()
        plt.xlabel('m/z')
        color_schema = ['b','g','r']
        for i in range(3):
            plt.plot(channels[i][0], self.data.intensities[i], color=color_schema[i])
        plt.show()
        self.readout_field['text'] = 'channel alignment bypassed!'
        return
        

    def merge_channels(self):
        '''
        Collapse the datasets from each of the 3 channels into a single unified mass
        spectrum.
        '''
        from scipy import signal
        # Get desired merge mode from main GUI window
        merge_mode = self.merge_mode.get()
        self.readout_field['text'] = 'performing channel merge: '+merge_mode+'...'
        self.data.merge_mode = merge_mode.upper()
        self.data.merge_parms = {}
        if merge_mode == 'Binned average':
            '''
            y values are average of intensities within bin edges across all channels
            '''
            # Get user-specified number of bins from main gui window
            n_bins = int(self.bin_size_field.get())
            self.data.merge_parms['n_bins'] = n_bins
            # Generate list of bin edges and centers
            bounds = (np.min(self.data.master_mz), np.max(self.data.master_mz))
            length = len(self.data.master_mz)
            bins = np.linspace(0, length, n_bins)
            bin_differences = np.diff(bins)
            bin_centers = [bins[i] + (bin_differences[i]/2) for i in range(len(bin_differences))]
            bin_counts = np.zeros_like(bin_centers)
            bin_sums = np.zeros_like(bin_centers)
            print('Binning spectra...')
            # Perform binning of data
            for idx, mz in enumerate(self.data.master_mz):
                mindex = find_nearest_member(bin_centers, idx) #!!!slow..........
                bin_sums[mindex] += self.data.master_int[idx]
                bin_counts[mindex] += 1
            bin_averages = [bin_sums[i]/bin_counts[i] for i in range(len(bin_sums))]
            y = bin_averages
            x = []
            cut = 0
            for idx, center in enumerate(bin_centers):
                floor = int(np.floor(center))
                remainder = center - floor
                low = self.data.master_mz[floor]
                try:
                    high = self.data.master_mz[floor+1]
                except IndexError:
                    print('Handled exception: EC 0001', center)
                    cut -= 1
                    continue
                diff = high-low
                value = low + (diff * remainder)
                x.append(value)
            # if any errors were encountered, remove a number of entries from the tail end of y equal to the number of errors encountered
            if cut < 0:
                y = y[:cut]
            x = np.array(x)
            y = np.array(y)
            self.data.merged_mz = x
#            y = np.array(signal.savgol_filter(y, 7, 1))    #Apply 1st order SavGol filter to smooth data
            self.data.merged_int = y
        elif merge_mode == 'Savitzky-Golay':
            '''
            Channel merge followed by brute force smoothing with high-order Savitzky Golay filter
            '''
            x = self.data.master_mz
            y_i = self.data.master_int
            y = np.array(signal.savgol_filter(y_i, 21, 7))
            self.data.merge_parms['SG order'] = 7
            self.data.merge_parms['SG window'] = 21
            self.data.merged_int = y
            self.data.merged_mz = x
        
        #Identify and analyze major peaks in merged data
        peaks, parameters = signal.find_peaks(self.data.merged_int,
                                              height=np.max(self.data.merged_int)*0.05,
                                              distance=int(20/3498*len(self.data.merged_mz)),
                                              prominence=np.max(self.data.merged_int)*0.05)
        x = []
        y = []
        fwhm = []
        resolution = []
        window_radius = 25
        for peak in peaks:
            peak_hm = []
            target = 25
            if peak < window_radius:
                x_window = self.data.merged_mz[:peak+window_radius]
                y_window = self.data.merged_int[:peak+window_radius]
                target = peak
            elif peak > len(self.data.merged_mz) - window_radius:
                x_window = self.data.merged_mz[peak-window_radius:]
                y_window = self.data.merged_int[peak-window_radius:]
            else:
                x_window = self.data.merged_mz[peak-window_radius:peak+window_radius]
                y_window = self.data.merged_int[peak-window_radius:peak+window_radius]
            y_max = np.max(y_window)
            hm = y_max/2
            i = target
            y_i = y_max
            while True:
                i-=1
                try:
                    y_ii = y_window[i]
                except IndexError:
                    low_bound = None
                    break
                if y_ii > hm: pass
                else:
                    low_bound = x_window[i]
                    break
            i = target
            y_i = y_max
            while True:
                i+=1
                try:
                    y_ii = y_window[i]
                except IndexError:
                    low_bound = None
                    break
                if y_ii > hm: pass
                else:
                    high_bound = x_window[i]
                    break
            if high_bound==None or low_bound==None: tfw=None
            else: tfw = high_bound - low_bound
            ci = tfw / 2.35482
            init = (y_window[window_radius], x_window[window_radius-1], ci)
#            fitted, popt = fit_distribution(x_window, y_window, init)
#            sigma = popt[2]
            fwhm.append(tfw) #!!! Currently just uses estimated fwhm, not gaussian estimated value. Gauss fit function needs polishing first
#            max_index = np.where(fitted==np.max(fitted))[0]
            x.append(x_window[window_radius])
            y.append(y_window[window_radius])
            resolution.append(x_window[window_radius] / tfw)
        # Save peak parameters to HimSimsData object
        self.data.peaks = {'mz':np.array(x),
                           'intensity':np.array(y),
                           'fwhm':np.array(fwhm),
                           'resolution':np.array(resolution)}
        # Plot merged spectrum
        plt.figure()
        plt.plot(self.data.merged_mz, self.data.merged_int)
        self.readout_field['text'] = 'channels merged succesfully'
        return

    def calibrate_spectra(self):

        def grab_peak(event):
            '''
            Fits gaussian to peak specified by user within matplotlib
            interactive interface window.

            Input:
            -------
            event : matplotlib.backend_bases.PickEvent
            user interaction event signalling peak to be grabbed for calibration
            '''
            xpos = event.xdata
            ypos = event.ydata
            if ypos < 0: return
            if type(xpos) == type(None) or type(ypos) == type(None):
                return anchor_peaks
            click_pos = (xpos, ypos)
            nearest_x = find_nearest_member(self.data.master_mz, xpos)
            target = 100
            lower = nearest_x - 100
            if lower < 0:
                target += lower
                lower = 0
            upper = nearest_x + 100
            if upper > len(self.data.master_int): upper = len(self.data.master_int)
            extracted_window = (self.data.master_mz[lower:upper], self.data.master_int[lower:upper])
            peaks, properties = signal.find_peaks(extracted_window[1], distance=15)
            nearest = peaks[find_nearest_member(peaks, target)]
            min_distance_peak = (extracted_window[0][nearest], extracted_window[1][nearest])
            print('click: %5f    peak: %5f'%(click_pos[0], min_distance_peak[0]))
            anchor_peaks.append(min_distance_peak)
            return

        def apply_calibration():
            '''
            Calculates 3rd order polynomial calibration coefficients for unified mass spectrum,
            based on user-specified peaks and lock-masses.
            '''
            
            import scipy.stats as stats
            
            def rmse(a,b):
                '''
                Calculates the root mean-square error between an experimental and
                target dataset. Either a or b may be either the target or experimental
                dataset. 
                '''
                a = np.array(a)
                b = np.array(b)
                rmse = np.sqrt(((a - b) ** 2).mean())
                return rmse
                
            plt.close()
            # Get and format calibration lock masses from helper window
            calibration_peaks = self.mass_entry_box.get(1.0, tkinter.END)
            calibration_peaks = calibration_peaks.split('\n')
            calibration_peaks.remove('')
            calibration_peaks = [float(mass) for mass in calibration_peaks]
            calibration_peaks.sort()
            x = [mass for (mass, intensity) in anchor_peaks]
            x.sort()
            x_i = [find_nearest_member(self.data.merged_mz, mass) for mass in x]
            y = calibration_peaks
            # Calculate 3rd order polynomial fitting coefficients
            p = np.polyfit(x,y,3)
            check_masses = apply_polynomial(x, p)
            # Calculate and save various fit parameters and metadata
            r, prob = stats.pearsonr(calibration_peaks, check_masses)
            self.data.calibration_params['order'] = 3
            self.data.calibration_params['coeffs'] = p
            self.data.calibration_params['RMSE'] = rmse(check_masses, calibration_peaks)
            self.data.calibration_params['r_squared'] = r**2
            self.data.calibration_params['anchors'] = [x,y]
            calibrated = apply_polynomial(self.data.merged_mz, p)
            self.data.calibrated_peaks = {**self.data.peaks}
            self.data.calibrated_peaks['mz'] = apply_polynomial(self.data.peaks['mz'], p)
            self.data.calibrated = calibrated
            # Identify and measure major peaks within calibration
                # Essentially refresh peak measurements made after channel merging
            peaks, parameters = signal.find_peaks(self.data.merged_int,
                                              height=np.max(self.data.merged_int)*0.05,
                                              distance=int(20/3498*len(self.data.merged_mz)),
                                              prominence=np.max(self.data.merged_int)*0.05)
            x = []
            y = []
            fwhm = []
            resolution = []
            window_radius = 25
            for peak in peaks:
                peak_hm = []
                target = 25
                if peak < window_radius:
                    x_window = self.data.merged_mz[:peak+window_radius]
                    y_window = self.data.merged_int[:peak+window_radius]
                    target = peak
                elif peak > len(self.data.merged_mz) - window_radius:
                    x_window = self.data.merged_mz[peak-window_radius:]
                    y_window = self.data.merged_int[peak-window_radius:]
                else:
                    x_window = self.data.merged_mz[peak-window_radius:peak+window_radius]
                    y_window = self.data.merged_int[peak-window_radius:peak+window_radius]
                y_max = np.max(y_window)
                hm = y_max/2
                i = target
                y_i = y_max
                while True:
                    i-=1
                    try:
                        y_ii = y_window[i]
                    except IndexError:
                        low_bound = None
                        break
                    if y_ii > hm: pass
                    else:
                        low_bound = x_window[i]
                        break
                i = target
                y_i = y_max
                while True:
                    i+=1
                    try:
                        y_ii = y_window[i]
                    except IndexError:
                        low_bound = None
                        break
                    if y_ii > hm: pass
                    else:
                        high_bound = x_window[i]
                        break
                if high_bound==None or low_bound==None: tfw=None
                else: tfw = high_bound - low_bound
                ci = tfw / 2.35482
                init = (y_window[window_radius], x_window[window_radius-1], ci)
    #            fitted, popt = fit_distribution(x_window, y_window, init)
    #            sigma = popt[2]
                fwhm.append(tfw) #!!! Currently just uses estimated fwhm, not gaussian estimated value. Gauss fit function needs polishing first
    #            max_index = np.where(fitted==np.max(fitted))[0]
                x.append(x_window[window_radius])
                y.append(y_window[window_radius])
                resolution.append(x_window[window_radius] / tfw)
            # Save calibrated data to HimSimsData object, and plot calibrated spectrum
            self.data.calibrated_peaks = {'mz':np.array(x),
                                          'intensity':np.array(y),
                                          'fwhm':np.array(fwhm),
                                          'resolution':np.array(resolution)}
            plt.figure()
            plt.plot(self.data.calibrated, self.data.merged_int)
            self.readout_field['text'] = 'spectral calibration complete'
            return
        
        self.readout_field['text'] = 'performing mass calibration...'
        calibration = tkinter.Toplevel()
        self.data.calibration_params = {}
        cal_mass_text = tkinter.StringVar()
        self.mass_entry_box = tkinter.Text(calibration, height=5, width=15)
        self.mass_entry_box.grid(row=0, column=0)
        self.calibrate_button = tkinter.Button(calibration, text='Calibrate!', command=apply_calibration)
        self.calibrate_button.grid(row=0, column=1)
        fig = plt.figure()
        plt.plot(self.data.merged_mz, self.data.merged_int)
        anchor_peaks = []
        cid_1 = fig.canvas.mpl_connect('button_release_event', grab_peak)

#Initialize GUI
root = tkinter.Tk()
main_window = CoRegistrationGUI(root)
root.mainloop()
